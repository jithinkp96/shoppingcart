import React, { Component } from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { StyleSheet, View ,Text, Image,TouchableOpacity} from 'react-native';
import { TextInput, ScrollView } from 'react-native-gesture-handler';

class ProductPage extends Component {
    constructor(props){
        super(props)
        this.state={
              produts: [
                {
                  name: "Salmon",
                  subtitle: "Fish items",
                  price:"50",
                  image:require('../assets/images/mathi.png'),
                  category:'Meats',
                },
                {
                    name: "Apple",
                    subtitle: "Fruits Items",
                    price:"105",
                    image:require('../assets/images/apple.jpg'),
                    category:'Vegetables and fruits'
                },
                {
                    name: "Carrot",
                    subtitle: "Veg items",
                    price:"65",
                    image:require('../assets/images/carrot.jpg'),
                    category:'Vegetables and fruits',
                },{
                name: "Orange",
                    subtitle: "Fruits Items",
                    price:"80",
                    image:require("../assets/images/orange.jpg"),
                    category:'Vegetables and fruits',
                },{
                name: "Pen",
                    subtitle: "Other products",
                    price:"10",
                    image:require("../assets/images/pen.jpg"),
                    category:'Other products'
                },{
                name: "Rice",
                subtitle: "Grosory items",
                price:"40",
                image:require("../assets/images/rice.jpg"),
                category:'Groseory'
            },{
            name: "Sanitizer",
            subtitle: "Cleaning items",
            price:"68",
            image:require("../assets/images/cleaning.jpg"),
            category:'Cleaning products'
        },
              ] ,
              selected:'select'
            
            }
        }
       
       onChangeHandler = (value) => {
           console.log('valueee',value)
            this.setState({selected:value})
        }

    
  render() {
    let category = [{
      value: 'Select',
    
    },{
      value:'Vegetables and fruits',  
    },
     {
      value: 'Meats',
    },{
        value:'Groseory',
    },{
        value:'Cleaning products',
    },{
        value:"Other products",
    }
];
let quantity = [{
    value: '500 GM',
  
  },{
      value:"250 GM"
  }

]

    return (
        
        <View style={styles.mainContainer}>
        <View style={styles.topbarView}>
        <View style={styles.dropdownView}>
      <Dropdown 
      containerStyle={{width:170,zIndex:60,height:60,justifyContent:"center",bottom:11, marginTop:0,padding:15,}}
      inputContainerStyle={{ borderBottomWidth: 0 }}
      value={category[0].value}
      
        data={category}
        baseColor="black"
        fontSize={12}
        dropdownPosition="0"
        labelfontColor="black" 
    
        onChangeText={(value => this.onChangeHandler(value))}
      />
       
        
     

     
      </View>
      <View>
<TextInput style={styles.searchTextInput}/>
      </View>
    
      <View>
      <Image style={styles.searchIcon} source ={require('../assets/images/search.png')} />
      </View>
      </View>
    <ScrollView style={styles.scrollView}>{this.state.produts.map((obj,key)=>{
    
         if(obj.category==this.state.selected){
        {console.log("true case")}
    return(
    
      <View style={styles.productCard} >
      
          <View style={styles.productImage}>
          <Image style={styles.productPng} source = {obj.image} />
          </View>
          <View style={styles.productDetails}>
    <Text style={{fontSize:15,fontWeight:"bold",marginTop:16,marginLeft:3}}>{obj.name}</Text>
    <Text style={{marginTop:3,marginLeft:3,fontSize:10}}>{obj.subtitle}</Text>
              <View style={styles.quantityDropdown} >
              <Dropdown 
      containerStyle={{width:80,height:20,bottom:10, marginTop:0,zIndex:60,justifyContent:"center",padding:9}}
      inputContainerStyle={{ borderBottomWidth: 0 }}
      value={quantity[0].value}
        data={quantity}
        baseColor="black"
        fontSize={10}
       
        labelfontColor="black"   
      />
      </View>
    <Text style={{fontSize:13,marginLeft:3,fontWeight:"bold",color:"red",marginTop:3}}>Rs. {obj.price}</Text>
      <View style={styles.cartButton}>
      <Text style={{justifyContent:"center"}}>CART NOW</Text>
      </View>
          </View>
      </View>
        
        // else{
        //     <View>
        //    {console.log("not workink")}
        //     </View>
        // }
    )
         }}
      ) }</ScrollView>
</View>
    );
  }
}
 
  const styles=StyleSheet.create({
      mainContainer:{
    marginTop:10,
    marginLeft:15,
    marginRight:20,
      },
      topbarView:{
        flexDirection:'row',
        borderColor:"#3E9906",
        borderWidth:1,
        borderStyle:"solid",
       
        borderRadius:5,
        marginBottom:20,
      },
      dropdownView:{
      
      
        width:160,
        height:40,
        backgroundColor:"#3E9906",
        justifyContent:"center",
        alignItems:'center',
        
       
        
      },
      searchTextInput:{
          width:130,
          fontSize:18,
          marginLeft:5,
          textAlign:"center",
          marginTop:6

      },
      searchIcon:{
        flex:1,
        resizeMode:"contain",
        borderRadius:2,
        aspectRatio:.6,
       height:40,
      },
      productCard:{
          
          height:160,
          borderWidth:1,
          borderStyle:"solid",
          borderRadius:4,
          borderColor:"green",
          marginBottom:5,
          flexDirection:"row",
          
          
      },
      productImage:{
          marginLeft:15,
          width:162,
          height:155,
          
      },
      productDetails:{
        width:162,
        height:155,
       

      },
      quantityDropdown:{
        marginTop:5,
        height:22,
          borderRadius:8,
          borderWidth:2,
          borderStyle:'solid',
          borderColor:'#CFD2CC',
         padding:5,
         
        
          width:70,
          justifyContent:"center",
        alignItems:'center',
  
      },
      cartButton:{
          marginTop:3,
        color:"#3E9906",
        width:100,
        borderRadius:10,
        borderColor:'red',
        backgroundColor:'#3E9906',
        height:30,
        justifyContent:"center",
        alignItems:'center'

    },
    productPng:{
        marginLeft:15,
        flex:1,
        resizeMode:"contain",
        borderRadius:2,
        aspectRatio:.5,
        height:40,

    
    },
    scrollView: {
       
       
        marginBottom:80
      },
   
  })  

export default ProductPage
