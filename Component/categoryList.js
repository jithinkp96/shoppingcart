import React, { useState } from 'react';
import { StyleSheet, Text, View,Image  } from 'react-native';
function categoryList(){
    return(
        <View style={styles.textpage}>
            <View  >
            <Image style={styles.image} source = {require('../assets/images/banner.png')} />
            </View>
            <View>
            <View style={{ flexDirection:"row",paddingTop:10}}>
            <View style={styles.widgets}>
            <Image style={styles.pngimage} source = {require('../assets/images/fish.png')} />
            <View style={styles.imageLabelView}>
            <Text style={styles.productName}>FISH {'\n'}ITMES</Text>
            </View>
            </View>
            <View style={styles.widgets}>
            <Image style={styles.pngimage} source = {require('../assets/images/grosery.png')} />
            <View style={styles.imageLabelView}>
            <Text style={styles.productName}>GROCERY {'\n'}ITMES</Text>
            </View>
</View>
</View>
<View style={{ flexDirection:"row"}}>
<View style={styles.widgets}>
<Image style={styles.pngimage} source = {require('../assets/images/cleaning.png')} />
<View style={styles.imageLabelView}>
            <Text style={styles.productName}>CLEANING {'\n'}MATERIALS</Text>
            </View>

</View>
<View style={styles.widgets}>
<Image style={styles.pngimage} source = {require('../assets/images/vegitabl.png')} />
<View style={styles.imageLabelView}>
            <Text style={styles.productName}>VEGETABLE {'\n'}FRUITS</Text>
            </View>


</View>
</View>
<View style={{ flexDirection:"row"}}>
<View style={styles.widgets}>
<Image style={styles.pngimage} source = {require('../assets/images/others.png')} />
<View style={styles.imageLabelView}>
            <Text style={styles.productName}>OTHER {'\n'}PRODUCTS</Text>
            </View>


</View>
<View style={styles.widgets}>
<Image style={styles.pngimage} source = {require('../assets/images/meat.png')} />
<View style={styles.imageLabelView}>
            <Text style={styles.productName}>MEAT {'\n'}ITEMS</Text>
            </View>


</View>
</View>
</View>
</View>
           
    )
}
const styles=StyleSheet.create({
   
    textpage:{
        padding:20,
    },
    image:{
        width:320,
        height:160,
        borderRadius:10,
    },
    widgets:{   
      width:150,
      height:130,
      borderStyle:"solid",
      borderColor:'green',
      borderWidth:1,
      margin:5,
      borderRadius:10,
      alignItems:'center',
      justifyContent:'center',
    },
    pngimage:{
        flex:1,
        resizeMode:"contain",
        borderRadius:2,
        aspectRatio:.65,
        
        justifyContent:'center',
       margin:2,
       
        
    },
    productName:{
        color:"green",

        justifyContent:"center",
        textAlign:"center",
       fontSize:12,
       fontWeight:"bold"
    },
    imageLabelView:{
        alignItems:'center',
        justifyContent:'center',
        width:147,
        height:40,    
    }


})
export default categoryList