import React, { useState } from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';  
import categoryList from './Component/categoryList'
import productPage from './Component/productPage'
const Stack=createStackNavigator()
export default function App() {
  
  return (
    <NavigationContainer>
      <Stack.Navigator>
      {/* <Stack.Screen name="Category list" component={categoryList} /> */}
      <Stack.Screen name="Product page" component={productPage} />
     </Stack.Navigator>
     
    </NavigationContainer>
    
  
  );
  }
